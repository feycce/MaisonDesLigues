$(document).ready( function () {
    let table, selectedRow;
    tableListener();
} );

function tableListener()
{
        table = $('#usersTable').DataTable();
     
        $('#usersTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('tr-color-selected') ) {
                $(this).removeClass('tr-color-selected');
                $("#btnEdit").attr("disabled", true);
                $("#btnDelete").attr("disabled", true);          
            }
            else {
                table.$('tr.tr-color-selected').removeClass('tr-color-selected');
                $(this).addClass('tr-color-selected');
                selectedRow = table.row($(this));
                $("#btnEdit").attr("disabled", false);
                $("#btnDelete").attr("disabled", false);
               
            }
        } );
     
        $('#button').click( function () {
            table.row('.selected').remove().draw( false );
        } );
}


function fillEditForm() //fill the editing form with selected user data
{
    $('#formNameEdit').val(selectedRow.data()[0]).trigger("change"); 
    $('#formPosEditDef').text(selectedRow.data()[1]).trigger("change"); 
    $('#formEmailEdit').val(selectedRow.data()[2]).trigger("change"); 

    $("#btnValidEdit").click(function() {
        $("#btnEdit").attr("disabled", true);
        $("#btnDelete").attr("disabled", true);
    });
}
//Deleting User functions
function fillDeleteForm() //Fill the delete form with selected user name
{
    
    $('#verificationDelete').text("Etes vous sur de vouloir supprimer " + selectedRow.data()[0] );
    
}

function deleteRow() //delete selected user row from table and draw a new table
{
    table
    .rows( '.tr-color-selected' )
    .remove()
    .draw();

}

function createUser()
{
    var level, name, pwd, email;
    
    level = $('#formPositionNew').val();
    name = $('#inputName').val();
    pwd = $('#inputPwdNewUser').val();
    email = $('#inputEmailNew').val();
    var targetFunction = 'insertUser';

    $.ajax({
        method: "POST",
        context: document.body,
        url: "model/userManagingModel/usersModel.php",
        data: {targFunc: targetFunction , level: level, name: name, pwd : pwd, email : email },
        dataType : "json"
      })
        .done(function( msg ) {
            var obj = $.parseJSON(msg);
            alert( "Data Saved: " + obj );
        });
}

function deleteUser()
{
    
    var emailToDelete = selectedRow.data()[2];
    var targetFunction = 'deleteUser';

    $.ajax({
        method: "POST",
        context: document.body,
        url: "model/userManagingModel/usersModel.php",
        data: {targFunc: targetFunction , email: emailToDelete },
        dataType: "json",
        async : "false"
      })
        .done(function(s) {
            console.log("After deleting user",s);
        });

    deleteRow();
}
function updateUser()
{
    var level, name, newEmail;
    
    level = $('#formPositionEdit').val();
    name = $('#formNameEdit').val();
    newEmail = $('#formEmailEdit').val();
    var oldEmail = selectedRow.data()[2];
    var targetFunction = 'updateUser';
    $.ajax({
        method: "POST",
        context: document.body,
        url: "model/userManagingModel/usersModel.php",
        data: {targFunc: targetFunction , level: level, name: name, email : newEmail, oldEmail: oldEmail },
        dataType: "json"
      })
        .done(function(s) {
            console.log("JS AJAX RESP UPDATE USER : "+ s);
        });

    deleteRow();
}

function checkMail()
{
    var mail = $('#inputEmailNew').val();
    var targetFunction = "mailExist";

    $.ajax({
        method: "POST",
        context: document.body,
        url: "model/userManagingModel/usersModel.php",
        data: {targFunc: targetFunction , email: mail},
        dataType: "json"
      })
        .done(function( msg ) {
            var obj = $.parseJSON(msg) // True if email already taken else False
          if(obj)
          {
              console.log(obj);
            $('#inputEmailNew').addClass("invalid"); 
          }
          else
          {
            $('#inputEmailNew').removeClass("invalid");
            $('#inputEmailNew').addClass("valid"); 
          }
    });

    
}




